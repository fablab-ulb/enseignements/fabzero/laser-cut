# Guide Lasersaur

## Préparation

Pour allumer la machine, tourner le bouton d’arrêt d’urgence dans le sens des flèches blanches :

![](/img/lasersaur/switch.jpg)

***

Retour à l’origine de la tête (bouton **Run homing cycle**) :

![](/img/lasersaur/homing.png)

***

Vérifier les statuts (bouton **Status**) :

![](/img/lasersaur/status.png)

***

Ouvrir un fichier SVG ou DXF (bouton **Open**) :

![](/img/lasersaur/open.png)

***

Vérifier l'échelle du dessin à l'aide de la grille en arrière-plan :

![](/img/lasersaur/grid.png)

Chaque carré fait 10 cm de côté.



## Positionnement

Installer le matériau dans la machine

***

Déplacer la tête de découpe au bon endroit :

* Relever la lentille pour éviter tout risque de collision !
* Positionner la tête au-dessus du matériau (bouton **Move** ou **Jog**)

![](/img/lasersaur/move.png) Move : positionnement libre

![](/img/lasersaur/jog.png) Jog : déplacement par pas de 5-10 cm

***

Régler la distance focale avec le support de 15 mm :

* Placer le support sous la lentille
* Dévisser la molette rouge
* Poser la lentille sur le support
* Visser la molette rouge (ne pas la serrer trop fort)

![](/img/lasersaur/focus.gif)

## Réglages

Si nécessaire, déplacer le dessin (bouton **Offset**) :

![](/img/lasersaur/offset.png)

***

Sélectionner une couleur ou une image (bouton **+**) :

![](/img/lasersaur/pass.png)

Régler la vitesse de découpe (en mm/minute) :

![](/img/lasersaur/feedrate.png)

Régler la puissance de découpe :

![](/img/lasersaur/power.png)

Des exemples de réglages sont donnés sur le [wiki Lasersaur](https://github.com/nortd/lasersaur/wiki/materials).

***

Vérifier que le dessin ne dépasse pas du matériau (bouton **Run bounding box**) :

![](/img/lasersaur/bounding.png)

***

Fermer le couvercle de la machine.



## Ne pas oublier !

Allumer le refroidisseur à eau (bouton noir) :

![](/img/lasersaur/chiller.png)

[comment]: # (Allumer l’extracteur de fumée (bouton vert) : ![](/img/filter.png))

Vérifier si l'extraction est bien allumé.

![](/img/general/room_extraction.jpeg)

Ouvrir la vanne d’air comprimé :

![](/img/lasersaur/valve.jpg)



## Démarrage

Vérifier que le bouton **Status** est en vert.

***

Démarrer la découpe (bouton **Run**) :

![](/img/lasersaur/run.png)

***

Si nécessaire, appuyer sur le bouton **Pause** :

![](/img/lasersaur/pause.png)

Il sera possible de reprendre le travail là où il s’est arrêté.

***

Pour annuler le travail, appuyer sur le bouton **Stop** :

![](/img/lasersaur/stop.png)

***

En cas de problème, presser le bouton d’arrêt d’urgence :

![](/img/lasersaur/panic.jpg)

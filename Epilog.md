# Guide Epilog Fusion Pro 32

## Spécifications

* Surface de découpe : 81 x 50 cm
* Hauteur maximum : 31 cm
* Puissance du LASER : 60 W
* Type de LASER : CO2 (infrarouge)

## Envoi d'un travail à la machine

### Ouvrir le fichier dans Inkscape

Si le format de fichier n'est pas SVG, importez-le dans Inkscape (Fichier -> Importer...).

Pour la découpe, vérifiez que l'épaisseur des traits est inférieure à 0,25 mm.

### Imprimer

La découpeuse LASER doit être allumée.

![](/img/epilog/print.png)

![](/img/epilog/printer.png)

Cliquez sur le bouton **Imprimer** et attendez l'ouverture du Dashboard Epilog.

Si rien ne se passe, démarrez le Dashboard Epilog manuellement via l'icône **Epilog** sur le bureau.

### Paramétrage

Commencez par donner un nom à votre travail :

![](/img/epilog/name.png)

Pour la découpe (ou la gavure vectorielle), sélectionnez **Vector** dans la liste **Process Type**.

N'utilisez l'option **Engrave** que si vous voulez graver une image matricielle.

![](/img/epilog/vector.png)

Réglez la vitesse, la puissance et la fréquence.

Vous pouvez trouver les réglages de vitesse et puissance dans ce [tableau des réglages en fonction des matériaux](https://www.epiloglaser.com/assets/downloads/fusion-material-settings.pdf), dans la colonne **60 watt** :

* Pour la découpe (vector), le réglage de la fréquence est aussi indiqué.
* Pour la gravure (engrave), c'est la résolution en DPI qui est indiquée.

Pour un réglage automatique du focus sur la surface du matériau, sélectionnez **Plunger** dans la liste **Auto-focus**.

Attention ! Ne pas utiliser cette option avec des matériaux souples ou fragiles (papier, textiles, ...).

![](/img/epilog/focus.png)

Avant d'envoyer le travail à la machine, positionnez le dessin à l'endroit voulu.

Attention, tout ce qui dépasse les pointillés roses ne sera pas découpé.

Il est possible d'élargir la zone de travail en déplaçant les lignes roses.

Si tout est prêt, appuyez sur le bouton **Print** :

![](/img/epilog/send.png)

### Démarrage

Le travail apparaît alors en haut de la liste sur l'écran de la machine.

Vérifiez que l'extracteur de fumée est allumé (le bouton s'illumine en vert quand il est allumé, en rouge quand il est éteint).

![](/img/epilog/epilog_extraction.jpeg)

Pour démarrer la découpe, sélectionnez-le puis appuyez sur "Play" (bouton noir et blanc).

![](/img/epilog/button.jpg)

## Problèmes courants

### Une partie de mon dessin ne s'affiche pas, ou il manque des couleurs

Ouvrez le fichier dans Inkscape et essayez de dégrouper tous les groupes :

* Sélectionnez les formes une par une, pour voir si elles sont groupées
* Objet -> Dégrouper
* Réessayez d'envoyer le fichier au Dashboard Epilog

### Mon dessin n'est pas à la bonne taille

Ouvrez le fichier dans Inkscape et vérifiez les unités du document :

* Fichier -> Propriétés du document
* Dans la case **Dimensions personnalisées** -> Unité : mm
* Fermez la fenêtre **Propriétés du document**
* Vérifiez les dimensions du dessin
* Réessayez d'envoyer le fichier au Dashboard Epilog

### Quand j'imprime un dessin, le Dashboard Epilog reste bloqué sur un autre projet

La mémoire du Dashboard Epilog est saturée.

* Cliquez sur le bouton en forme de roue dentée (en haut à droite)
* Dans l'onglet **Database**, cliquez sur **Clear All Jobs**
* Fermez la fenêtre **Epilog Settings**
* Réessayez d'envoyer le fichier au Dashboard Epilog

![](/img/epilog/settings.png)

![](/img/epilog/database.png)

# Exercices

[Fichier de base - SVG](https://gitlab.com/fablab-ulb/enseignements/fabzero/laser-cut/-/blob/main/files/Exercices/House.svg?ref_type=heads)

[Fichier de base - DXF](https://gitlab.com/fablab-ulb/enseignements/fabzero/laser-cut/-/blob/main/files/Exercices/House.dxf?ref_type=heads)

1. Ajouter des couleurs aux lignes et remplissages
2. Corriger les erreurs qui y sont caché (les erreurs ne vont pas faire échouer la découpe)

![](/img/exercices/exercices.png)

***

<details>
<summary>Aide mémoire - Click to expand</summary>

* Vectorisation des objets
* Distance entre les coordonées (0,0) et le dessin

</details>

***

# Découpeuses laser

## Précautions indispensables

### Avant

* Connaître avec certitude quel est le matériau à découper !
* Toujours activer l’air comprimé (sauf pour l'Epilog, c'est automatique)
* Toujours allumer l’extracteur de fumée !
* Savoir où est le bouton d’arrêt d’urgence
* Savoir où trouver un extincteur au CO2

### Pendant

* Porter des lunettes de protection adéquates
* Ne pas regarder fixement l'impact du faisceau laser
* Rester à proximité de la machine jusqu'à la fin de la découpe

### Après

* Ne pas ouvrir la machine tant qu'il y a de la fumée à l'intérieur
* Enlever les résidus de la découpe (si besoin, utiliser un aspirateur)

## Matériaux pour la découpe

### Matériaux recommandés

* Acrylique (PMMA/Plexiglas)
* Bois brut
* Bois contreplaqué (plywood/multiplex)
* Textiles
* Papier, carton

Attention ! Le carton ondulé avec 3 épaisseurs (ou plus) peut facilement prendre feu !

### Matériaux déconseillés

Ces matériaux peuvent être découpés **en petite quantité** et seulement si l'épaisseur est faible (max. 3mm).

* MDF : fumée épaisse et très nocive à long terme
* ABS, Polystyrène (PS) : fond facilement, fumée nocive
* PE, PET : fond facilement
* Polypropylène (PP) : fond facilement, produit beaucoup de poussière
* Polycarbonate (PC), Nylon (PA), Polyurethane (PU) : fumée nocive
* Composites à base de fibres : difficile à découper, poussières très nocives

### Matériaux interdits !

* PVC : fumée acide et très nocive
* Cuivre : réfléchit totalement le laser
* Téflon (PTFE) : fumée acide et très nocive
* Résine phénolique, époxy : fumée très nocive
* Vinyl, simili-cuir : peut contenir de la chlorine (fumée acide et très nocive)
* Cuir animal : produit beaucoup de poussière, odeur persistante

## Processus

### Découpe (vector)

La découpe se fait suivant un tracé vectoriel. Il faut donc importer un fichier vectoriel (par exemple au format .svg).

Pour les matériaux épais, il est souvent nécéssaire de faire plusieurs passages pour arriver à découper à travers.

### Gravure vectorielle

Même technique que pour la découpe mais avec moins de puissance. Les traits sont donc gravés à une faible profondeur.

### Gravure matricielle (engrave)

La gravure matricielle permet de graver un dessin à la surface du matériau.

Avec ce processus, la machine fonctionne comme une imprimante. Le dessin sera imprimé sous forme de trame.

La machine va adapter l'intensité du laser en fonction de la luminosité du dessin. Les parties sombres seront gravées plus fort que les parties claires.

## Réglages

Les deux réglages indispensables sont la **vitesse** et la **puissance**. Attention à ne pas confondre les deux !

* Une puissance élevée implique plus de chance de découper la matière en une passe, mais aussi plus de risque de brûler la matière !
* Une vitesse élevé implique que le travail sera fini plus vite, mais aussi un risque de ne pas couper à travers la matière.

Par exemple :

* Pour un matériau épais et dense (comme du bois), on choisira une puissance élevée avec une faible vitesse.
* Pour un matériau fin (comme du papier ou du carton), on choisira une puissance faible avec une vitesse élevée.

Pour savoir quel est le réglage optimal pour votre matériau, consultez les grilles de test présentes au fablab.

S'il n'y a pas de grille de test correspondant à votre matériau, il est fortement recommandé de faire des tests vous-même avant de commencer une découpe plus conséquente.

# Machines du fablab

## Epilog Fusion Pro 32

### Spécifications

* Surface de découpe : 81 x 50 cm
* Hauteur maximum : 31 cm
* Puissance du LASER : 60 W
* Type de LASER : CO2 (infrarouge)
* Logiciel : Epilog Dashboard

### Manuel

[Manuel de l'Epilog Fusion Pro](Epilog.md)

## Lasersaur

### Spécifications

* Surface de découpe : 120 x 60 cm
* Hauteur maximum : 12 cm
* Vitesse maximum : 4000 mm/min
* Puissance du LASER : 100 W
* Type de LASER : CO2 (infrarouge)
* Logiciel : Driveboard App

### Manuel

[Manuel de la Lasersaur](Lasersaur.md)

## Full Spectrum Muse

### Spécifications

* Surface de découpe : 50 x 30 cm
* Hauteur maximum : 6 cm
* Puissance du LASER : 40 W
* Type de LASER : CO2 (infrarouge) + pointeur rouge
* Logiciel : Retina Engrave

### Manuel

Pour apprendre à utiliser Retina Engrave, suivez les [tutoriels vidéo](https://www.youtube.com/playlist?list=PL_1I1UNQ4oGa0w55C772Y1mC6F4f3ZcG6) (en anglais).

Pour les réglages, consultez [le site de Full Spectrum Laser](http://laser101.fslaser.com/materialtest).

## Ressources

[Laser Cutting tutorial by Massimo Menichinelli](https://www.slideshare.net/openp2pdesign/fab-academy-2015-laser-cutting)
